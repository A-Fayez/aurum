use async_std::sync::Mutex;
use async_trait::async_trait;
use rusoto_core::RusotoError;
use rusoto_s3::*;
use std::cell::RefCell;
use std::collections::HashMap;
use std::sync::Arc;
use tokio::io::AsyncReadExt;

#[derive(Clone)]
pub struct S3MockClient {
    buckets: Arc<Mutex<RefCell<HashMap<String, Objects>>>>,
}

struct Objects(HashMap<String, Vec<u8>>);

impl S3MockClient {
    pub fn new() -> Self {
        Self {
            buckets: Arc::new(Mutex::new(RefCell::new(HashMap::new()))),
        }
    }
}

#[async_trait]
impl S3 for S3MockClient {
    async fn abort_multipart_upload(
        &self,
        _input: AbortMultipartUploadRequest,
    ) -> Result<AbortMultipartUploadOutput, RusotoError<AbortMultipartUploadError>> {
        unimplemented!();
    }

    async fn complete_multipart_upload(
        &self,
        _input: CompleteMultipartUploadRequest,
    ) -> Result<CompleteMultipartUploadOutput, RusotoError<CompleteMultipartUploadError>> {
        unimplemented!();
    }

    async fn copy_object(
        &self,
        _input: CopyObjectRequest,
    ) -> Result<CopyObjectOutput, RusotoError<CopyObjectError>> {
        unimplemented!();
    }

    async fn create_bucket(
        &self,
        input: CreateBucketRequest,
    ) -> Result<CreateBucketOutput, RusotoError<CreateBucketError>> {
        let guard = self.buckets.lock().await;
        let mut buckets = (*guard).borrow_mut();
        buckets.insert(input.bucket, Objects(HashMap::new()));
        Ok(CreateBucketOutput { location: None })
    }

    async fn create_multipart_upload(
        &self,
        _input: CreateMultipartUploadRequest,
    ) -> Result<CreateMultipartUploadOutput, RusotoError<CreateMultipartUploadError>> {
        unimplemented!();
    }

    async fn delete_bucket(
        &self,
        _input: DeleteBucketRequest,
    ) -> Result<(), RusotoError<DeleteBucketError>> {
        unimplemented!();
    }

    async fn delete_bucket_analytics_configuration(
        &self,
        _input: DeleteBucketAnalyticsConfigurationRequest,
    ) -> Result<(), RusotoError<DeleteBucketAnalyticsConfigurationError>> {
        unimplemented!();
    }

    async fn delete_bucket_cors(
        &self,
        _input: DeleteBucketCorsRequest,
    ) -> Result<(), RusotoError<DeleteBucketCorsError>> {
        unimplemented!();
    }

    async fn delete_bucket_encryption(
        &self,
        _input: DeleteBucketEncryptionRequest,
    ) -> Result<(), RusotoError<DeleteBucketEncryptionError>> {
        unimplemented!();
    }

    async fn delete_bucket_inventory_configuration(
        &self,
        _input: DeleteBucketInventoryConfigurationRequest,
    ) -> Result<(), RusotoError<DeleteBucketInventoryConfigurationError>> {
        unimplemented!();
    }

    async fn delete_bucket_lifecycle(
        &self,
        _input: DeleteBucketLifecycleRequest,
    ) -> Result<(), RusotoError<DeleteBucketLifecycleError>> {
        unimplemented!();
    }

    async fn delete_bucket_metrics_configuration(
        &self,
        _input: DeleteBucketMetricsConfigurationRequest,
    ) -> Result<(), RusotoError<DeleteBucketMetricsConfigurationError>> {
        unimplemented!();
    }

    async fn delete_bucket_policy(
        &self,
        _input: DeleteBucketPolicyRequest,
    ) -> Result<(), RusotoError<DeleteBucketPolicyError>> {
        unimplemented!();
    }

    async fn delete_bucket_replication(
        &self,
        _input: DeleteBucketReplicationRequest,
    ) -> Result<(), RusotoError<DeleteBucketReplicationError>> {
        unimplemented!();
    }

    async fn delete_bucket_tagging(
        &self,
        _input: DeleteBucketTaggingRequest,
    ) -> Result<(), RusotoError<DeleteBucketTaggingError>> {
        unimplemented!();
    }

    async fn delete_bucket_website(
        &self,
        _input: DeleteBucketWebsiteRequest,
    ) -> Result<(), RusotoError<DeleteBucketWebsiteError>> {
        unimplemented!();
    }

    async fn delete_object(
        &self,
        _input: DeleteObjectRequest,
    ) -> Result<DeleteObjectOutput, RusotoError<DeleteObjectError>> {
        unimplemented!();
    }

    async fn delete_object_tagging(
        &self,
        _input: DeleteObjectTaggingRequest,
    ) -> Result<DeleteObjectTaggingOutput, RusotoError<DeleteObjectTaggingError>> {
        unimplemented!();
    }

    async fn delete_objects(
        &self,
        _input: DeleteObjectsRequest,
    ) -> Result<DeleteObjectsOutput, RusotoError<DeleteObjectsError>> {
        unimplemented!();
    }

    async fn delete_public_access_block(
        &self,
        _input: DeletePublicAccessBlockRequest,
    ) -> Result<(), RusotoError<DeletePublicAccessBlockError>> {
        unimplemented!();
    }

    async fn get_bucket_accelerate_configuration(
        &self,
        _input: GetBucketAccelerateConfigurationRequest,
    ) -> Result<
        GetBucketAccelerateConfigurationOutput,
        RusotoError<GetBucketAccelerateConfigurationError>,
    > {
        unimplemented!();
    }

    async fn get_bucket_acl(
        &self,
        _input: GetBucketAclRequest,
    ) -> Result<GetBucketAclOutput, RusotoError<GetBucketAclError>> {
        unimplemented!();
    }

    async fn get_bucket_analytics_configuration(
        &self,
        _input: GetBucketAnalyticsConfigurationRequest,
    ) -> Result<
        GetBucketAnalyticsConfigurationOutput,
        RusotoError<GetBucketAnalyticsConfigurationError>,
    > {
        unimplemented!();
    }

    async fn get_bucket_cors(
        &self,
        _input: GetBucketCorsRequest,
    ) -> Result<GetBucketCorsOutput, RusotoError<GetBucketCorsError>> {
        unimplemented!();
    }

    async fn get_bucket_encryption(
        &self,
        _input: GetBucketEncryptionRequest,
    ) -> Result<GetBucketEncryptionOutput, RusotoError<GetBucketEncryptionError>> {
        unimplemented!();
    }

    async fn get_bucket_inventory_configuration(
        &self,
        _input: GetBucketInventoryConfigurationRequest,
    ) -> Result<
        GetBucketInventoryConfigurationOutput,
        RusotoError<GetBucketInventoryConfigurationError>,
    > {
        unimplemented!();
    }

    async fn get_bucket_lifecycle(
        &self,
        _input: GetBucketLifecycleRequest,
    ) -> Result<GetBucketLifecycleOutput, RusotoError<GetBucketLifecycleError>> {
        unimplemented!();
    }

    async fn get_bucket_lifecycle_configuration(
        &self,
        _input: GetBucketLifecycleConfigurationRequest,
    ) -> Result<
        GetBucketLifecycleConfigurationOutput,
        RusotoError<GetBucketLifecycleConfigurationError>,
    > {
        unimplemented!();
    }

    async fn get_bucket_location(
        &self,
        _input: GetBucketLocationRequest,
    ) -> Result<GetBucketLocationOutput, RusotoError<GetBucketLocationError>> {
        unimplemented!();
    }

    async fn get_bucket_logging(
        &self,
        _input: GetBucketLoggingRequest,
    ) -> Result<GetBucketLoggingOutput, RusotoError<GetBucketLoggingError>> {
        unimplemented!();
    }

    async fn get_bucket_metrics_configuration(
        &self,
        _input: GetBucketMetricsConfigurationRequest,
    ) -> Result<GetBucketMetricsConfigurationOutput, RusotoError<GetBucketMetricsConfigurationError>>
    {
        unimplemented!();
    }

    async fn get_bucket_notification(
        &self,
        _input: GetBucketNotificationConfigurationRequest,
    ) -> Result<NotificationConfigurationDeprecated, RusotoError<GetBucketNotificationError>> {
        unimplemented!();
    }

    async fn get_bucket_notification_configuration(
        &self,
        _input: GetBucketNotificationConfigurationRequest,
    ) -> Result<NotificationConfiguration, RusotoError<GetBucketNotificationConfigurationError>>
    {
        unimplemented!();
    }

    async fn get_bucket_policy(
        &self,
        _input: GetBucketPolicyRequest,
    ) -> Result<GetBucketPolicyOutput, RusotoError<GetBucketPolicyError>> {
        unimplemented!();
    }

    async fn get_bucket_policy_status(
        &self,
        _input: GetBucketPolicyStatusRequest,
    ) -> Result<GetBucketPolicyStatusOutput, RusotoError<GetBucketPolicyStatusError>> {
        unimplemented!();
    }

    async fn get_bucket_replication(
        &self,
        _input: GetBucketReplicationRequest,
    ) -> Result<GetBucketReplicationOutput, RusotoError<GetBucketReplicationError>> {
        unimplemented!();
    }

    async fn get_bucket_request_payment(
        &self,
        _input: GetBucketRequestPaymentRequest,
    ) -> Result<GetBucketRequestPaymentOutput, RusotoError<GetBucketRequestPaymentError>> {
        unimplemented!();
    }

    async fn get_bucket_tagging(
        &self,
        _input: GetBucketTaggingRequest,
    ) -> Result<GetBucketTaggingOutput, RusotoError<GetBucketTaggingError>> {
        unimplemented!();
    }

    async fn get_bucket_versioning(
        &self,
        _input: GetBucketVersioningRequest,
    ) -> Result<GetBucketVersioningOutput, RusotoError<GetBucketVersioningError>> {
        unimplemented!();
    }

    async fn get_bucket_website(
        &self,
        _input: GetBucketWebsiteRequest,
    ) -> Result<GetBucketWebsiteOutput, RusotoError<GetBucketWebsiteError>> {
        unimplemented!();
    }

    async fn get_object(
        &self,
        input: GetObjectRequest,
    ) -> Result<GetObjectOutput, RusotoError<GetObjectError>> {
        let guard = self.buckets.lock().await;
        let buckets = (*guard).borrow();
        if let Some(bucket) = buckets.get(&input.bucket) {
            if let Some(object) = bucket.0.get(&input.key) {
                Ok(GetObjectOutput {
                    body: Some(object.clone().into()),
                    ..Default::default()
                })
            } else {
                Err(RusotoError::Service(GetObjectError::NoSuchKey(input.key)))
            }
        } else {
            Err(RusotoError::Service(GetObjectError::NoSuchKey(input.key)))
        }
    }

    async fn get_object_acl(
        &self,
        _input: GetObjectAclRequest,
    ) -> Result<GetObjectAclOutput, RusotoError<GetObjectAclError>> {
        unimplemented!();
    }

    async fn get_object_legal_hold(
        &self,
        _input: GetObjectLegalHoldRequest,
    ) -> Result<GetObjectLegalHoldOutput, RusotoError<GetObjectLegalHoldError>> {
        unimplemented!();
    }

    async fn get_object_lock_configuration(
        &self,
        _input: GetObjectLockConfigurationRequest,
    ) -> Result<GetObjectLockConfigurationOutput, RusotoError<GetObjectLockConfigurationError>>
    {
        unimplemented!();
    }

    async fn get_object_retention(
        &self,
        _input: GetObjectRetentionRequest,
    ) -> Result<GetObjectRetentionOutput, RusotoError<GetObjectRetentionError>> {
        unimplemented!();
    }

    async fn get_object_tagging(
        &self,
        _input: GetObjectTaggingRequest,
    ) -> Result<GetObjectTaggingOutput, RusotoError<GetObjectTaggingError>> {
        unimplemented!();
    }

    async fn get_object_torrent(
        &self,
        _input: GetObjectTorrentRequest,
    ) -> Result<GetObjectTorrentOutput, RusotoError<GetObjectTorrentError>> {
        unimplemented!();
    }

    async fn get_public_access_block(
        &self,
        _input: GetPublicAccessBlockRequest,
    ) -> Result<GetPublicAccessBlockOutput, RusotoError<GetPublicAccessBlockError>> {
        unimplemented!();
    }

    async fn head_bucket(
        &self,
        _input: HeadBucketRequest,
    ) -> Result<(), RusotoError<HeadBucketError>> {
        unimplemented!();
    }

    async fn head_object(
        &self,
        _input: HeadObjectRequest,
    ) -> Result<HeadObjectOutput, RusotoError<HeadObjectError>> {
        unimplemented!();
    }

    async fn list_bucket_analytics_configurations(
        &self,
        _input: ListBucketAnalyticsConfigurationsRequest,
    ) -> Result<
        ListBucketAnalyticsConfigurationsOutput,
        RusotoError<ListBucketAnalyticsConfigurationsError>,
    > {
        unimplemented!();
    }

    async fn list_bucket_inventory_configurations(
        &self,
        _input: ListBucketInventoryConfigurationsRequest,
    ) -> Result<
        ListBucketInventoryConfigurationsOutput,
        RusotoError<ListBucketInventoryConfigurationsError>,
    > {
        unimplemented!();
    }

    async fn list_bucket_metrics_configurations(
        &self,
        _input: ListBucketMetricsConfigurationsRequest,
    ) -> Result<
        ListBucketMetricsConfigurationsOutput,
        RusotoError<ListBucketMetricsConfigurationsError>,
    > {
        unimplemented!();
    }

    async fn list_buckets(&self) -> Result<ListBucketsOutput, RusotoError<ListBucketsError>> {
        unimplemented!();
    }

    async fn list_multipart_uploads(
        &self,
        _input: ListMultipartUploadsRequest,
    ) -> Result<ListMultipartUploadsOutput, RusotoError<ListMultipartUploadsError>> {
        unimplemented!();
    }

    async fn list_object_versions(
        &self,
        _input: ListObjectVersionsRequest,
    ) -> Result<ListObjectVersionsOutput, RusotoError<ListObjectVersionsError>> {
        unimplemented!();
    }

    async fn list_objects(
        &self,
        _input: ListObjectsRequest,
    ) -> Result<ListObjectsOutput, RusotoError<ListObjectsError>> {
        unimplemented!();
    }

    async fn list_objects_v2(
        &self,
        _input: ListObjectsV2Request,
    ) -> Result<ListObjectsV2Output, RusotoError<ListObjectsV2Error>> {
        unimplemented!();
    }

    async fn list_parts(
        &self,
        _input: ListPartsRequest,
    ) -> Result<ListPartsOutput, RusotoError<ListPartsError>> {
        unimplemented!();
    }

    async fn put_bucket_accelerate_configuration(
        &self,
        _input: PutBucketAccelerateConfigurationRequest,
    ) -> Result<(), RusotoError<PutBucketAccelerateConfigurationError>> {
        unimplemented!();
    }

    async fn put_bucket_acl(
        &self,
        _input: PutBucketAclRequest,
    ) -> Result<(), RusotoError<PutBucketAclError>> {
        unimplemented!();
    }

    async fn put_bucket_analytics_configuration(
        &self,
        _input: PutBucketAnalyticsConfigurationRequest,
    ) -> Result<(), RusotoError<PutBucketAnalyticsConfigurationError>> {
        unimplemented!();
    }

    async fn put_bucket_cors(
        &self,
        _input: PutBucketCorsRequest,
    ) -> Result<(), RusotoError<PutBucketCorsError>> {
        unimplemented!();
    }

    async fn put_bucket_encryption(
        &self,
        _input: PutBucketEncryptionRequest,
    ) -> Result<(), RusotoError<PutBucketEncryptionError>> {
        unimplemented!();
    }

    async fn put_bucket_inventory_configuration(
        &self,
        _input: PutBucketInventoryConfigurationRequest,
    ) -> Result<(), RusotoError<PutBucketInventoryConfigurationError>> {
        unimplemented!();
    }

    async fn put_bucket_lifecycle(
        &self,
        _input: PutBucketLifecycleRequest,
    ) -> Result<(), RusotoError<PutBucketLifecycleError>> {
        unimplemented!();
    }

    async fn put_bucket_lifecycle_configuration(
        &self,
        _input: PutBucketLifecycleConfigurationRequest,
    ) -> Result<(), RusotoError<PutBucketLifecycleConfigurationError>> {
        unimplemented!();
    }

    async fn put_bucket_logging(
        &self,
        _input: PutBucketLoggingRequest,
    ) -> Result<(), RusotoError<PutBucketLoggingError>> {
        unimplemented!();
    }

    async fn put_bucket_metrics_configuration(
        &self,
        _input: PutBucketMetricsConfigurationRequest,
    ) -> Result<(), RusotoError<PutBucketMetricsConfigurationError>> {
        unimplemented!();
    }

    async fn put_bucket_notification(
        &self,
        _input: PutBucketNotificationRequest,
    ) -> Result<(), RusotoError<PutBucketNotificationError>> {
        unimplemented!();
    }

    async fn put_bucket_notification_configuration(
        &self,
        _input: PutBucketNotificationConfigurationRequest,
    ) -> Result<(), RusotoError<PutBucketNotificationConfigurationError>> {
        unimplemented!();
    }

    async fn put_bucket_policy(
        &self,
        _input: PutBucketPolicyRequest,
    ) -> Result<(), RusotoError<PutBucketPolicyError>> {
        unimplemented!();
    }

    async fn put_bucket_replication(
        &self,
        _input: PutBucketReplicationRequest,
    ) -> Result<(), RusotoError<PutBucketReplicationError>> {
        unimplemented!();
    }

    async fn put_bucket_request_payment(
        &self,
        _input: PutBucketRequestPaymentRequest,
    ) -> Result<(), RusotoError<PutBucketRequestPaymentError>> {
        unimplemented!();
    }

    async fn put_bucket_tagging(
        &self,
        _input: PutBucketTaggingRequest,
    ) -> Result<(), RusotoError<PutBucketTaggingError>> {
        unimplemented!();
    }

    async fn put_bucket_versioning(
        &self,
        _input: PutBucketVersioningRequest,
    ) -> Result<(), RusotoError<PutBucketVersioningError>> {
        unimplemented!();
    }

    async fn put_bucket_website(
        &self,
        _input: PutBucketWebsiteRequest,
    ) -> Result<(), RusotoError<PutBucketWebsiteError>> {
        unimplemented!();
    }

    async fn put_object(
        &self,
        input: PutObjectRequest,
    ) -> Result<PutObjectOutput, RusotoError<PutObjectError>> {
        let mut body = Vec::new();
        input
            .body
            .unwrap()
            .into_async_read()
            .read_to_end(&mut body)
            .await
            .unwrap();
        let guard = self.buckets.lock().await;
        let mut buckets = (*guard).borrow_mut();
        if let Some(bucket) = buckets.get_mut(&input.bucket) {
            bucket.0.insert(input.key, body);
        }
        Ok(PutObjectOutput {
            ..Default::default()
        })
    }

    async fn put_object_acl(
        &self,
        _input: PutObjectAclRequest,
    ) -> Result<PutObjectAclOutput, RusotoError<PutObjectAclError>> {
        unimplemented!();
    }

    async fn put_object_legal_hold(
        &self,
        _input: PutObjectLegalHoldRequest,
    ) -> Result<PutObjectLegalHoldOutput, RusotoError<PutObjectLegalHoldError>> {
        unimplemented!();
    }

    async fn put_object_lock_configuration(
        &self,
        _input: PutObjectLockConfigurationRequest,
    ) -> Result<PutObjectLockConfigurationOutput, RusotoError<PutObjectLockConfigurationError>>
    {
        unimplemented!();
    }

    async fn put_object_retention(
        &self,
        _input: PutObjectRetentionRequest,
    ) -> Result<PutObjectRetentionOutput, RusotoError<PutObjectRetentionError>> {
        unimplemented!();
    }

    async fn put_object_tagging(
        &self,
        _input: PutObjectTaggingRequest,
    ) -> Result<PutObjectTaggingOutput, RusotoError<PutObjectTaggingError>> {
        unimplemented!();
    }

    async fn put_public_access_block(
        &self,
        _input: PutPublicAccessBlockRequest,
    ) -> Result<(), RusotoError<PutPublicAccessBlockError>> {
        unimplemented!();
    }

    async fn restore_object(
        &self,
        _input: RestoreObjectRequest,
    ) -> Result<RestoreObjectOutput, RusotoError<RestoreObjectError>> {
        unimplemented!();
    }

    async fn select_object_content(
        &self,
        _input: SelectObjectContentRequest,
    ) -> Result<SelectObjectContentOutput, RusotoError<SelectObjectContentError>> {
        unimplemented!();
    }

    async fn upload_part(
        &self,
        _input: UploadPartRequest,
    ) -> Result<UploadPartOutput, RusotoError<UploadPartError>> {
        unimplemented!();
    }

    async fn upload_part_copy(
        &self,
        _input: UploadPartCopyRequest,
    ) -> Result<UploadPartCopyOutput, RusotoError<UploadPartCopyError>> {
        unimplemented!();
    }
}

# Aurum

> A cloud based Arch Linux build system

## Getting started

Requirements:
- [docker](https://www.docker.com/get-started)
- [k3d](https://k3d.io/)
- [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
- [tilt](https://docs.tilt.dev/install.html)

```bash
./kube/bootstrap
tilt up
```

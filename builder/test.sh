#!/bin/sh
set -ue

docker build -t builder .
docker network create builder

trap 'docker stop minio; docker network rm builder' EXIT

docker run -d --rm \
  -p 9000:9000 \
  -e MINIO_ACCESS_KEY=user \
  -e MINIO_SECRET_KEY=password \
  --network=builder \
  --name=minio \
  minio/minio server /data

docker run \
  --rm \
  -e AWS_ACCESS_KEY_ID=user \
  -e AWS_SECRET_ACCESS_KEY=password \
  --network=builder \
  amazon/aws-cli \
    --endpoint-url http://minio:9000 s3 mb s3://repo

docker run \
  --rm \
  -e AWS_ACCESS_KEY_ID=user \
  -e AWS_SECRET_ACCESS_KEY=password \
  -e ENDPOINT_URL=http://minio:9000 \
  -e BUCKET=s3://repo \
  --network=builder \
  "builder" \
    build.sh aurutils 6BC26A17B9B7018A

docker run \
  --rm \
  -e AWS_ACCESS_KEY_ID=user \
  -e AWS_SECRET_ACCESS_KEY=password \
  --network=builder \
  amazon/aws-cli \
    --endpoint-url http://minio:9000 s3 ls s3://repo | grep "aurutils.*\.pkg\.tar\..*"

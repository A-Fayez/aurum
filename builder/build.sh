#!/bin/bash
set -ueo pipefail
# shellcheck disable=SC2154
trap 's=$?; echo "$0: Error on line "$LINENO": $BASH_COMMAND"; exit $s' ERR
IFS=$'\n\t'

PACKAGE=${1:?}
ENDPOINT_URL=${ENDPOINT_URL:?}
BUCKET=${BUCKET:?}
shift

if [ -n "${1:-}" ]; then
  gpg --keyserver hkp://keys.gnupg.net:80 --recv-keys "${@}"
fi

curl -SsfL -o "/tmp/${PACKAGE}.tar.gz"  "https://aur.archlinux.org/cgit/aur.git/snapshot/${PACKAGE}.tar.gz"
tar -C /tmp -xf "/tmp/${PACKAGE}.tar.gz"
cd "/tmp/${PACKAGE}"
sudo pacman --noconfirm --noprogressbar --needed -Syu
makepkg --syncdeps --noconfirm --noprogressbar

if ! aws --endpoint-url "${ENDPOINT_URL}" s3 ls "${BUCKET}" >/dev/null 2>&1; then
  echo "Creating ${BUCKET} bucket at ${ENDPOINT_URL}"
  aws --endpoint-url "${ENDPOINT_URL}" s3 mb "${BUCKET}"
fi

aws --endpoint-url "${ENDPOINT_URL}" s3 cp "/tmp/${PACKAGE}/"*.pkg.tar.* "${BUCKET}"

use crate::package::PkgInfo;
use std::io::Write;
use tar::{Builder, Header};

pub struct Database<W: Write> {
    db: Builder<W>,
}

impl Database<Vec<u8>> {
    pub fn new() -> Database<Vec<u8>> {
        Self {
            db: Builder::new(Vec::new()),
        }
    }
}

impl<W: Write> Database<W> {
    pub fn add_package_to_db(
        &mut self,
        pkg_info: PkgInfo,
        filename: &str,
        md5sum: &str,
        sha256sum: &str,
    ) -> std::io::Result<()> {
        let data_string: String =
            Self::parse_pkginfo_to_db_desc(&pkg_info, filename, md5sum, sha256sum);
        let data: &[u8] = data_string.as_bytes();

        let mut header = Header::new_gnu();
        header.set_size(data.len() as u64);
        header.set_cksum();
        header.set_mode(0o644);
        self.db.append_data(
            &mut header,
            format!("{}-{}/desc", pkg_info.pkgname, pkg_info.pkgver),
            data,
        )?;
        Ok(())
    }

    fn parse_pkginfo_to_db_desc(
        pkg_info: &PkgInfo,
        filename: &str,
        md5sum: &str,
        sha256sum: &str,
    ) -> String {
        let mut desc: String = format!("%NAME%\n{}\n", &pkg_info.pkgname);
        desc.push_str(&format!("\n%FILENAME%\n{}\n", filename));
        desc.push_str(&Self::parser_read_single_value(
            pkg_info.pkgbase.as_ref(),
            "BASE",
        ));
        desc.push_str(&format!("\n%VERSION%\n{}\n", &pkg_info.pkgver));
        desc.push_str(&Self::parser_read_single_value(
            pkg_info.pkgdesc.as_ref(),
            "DESC",
        ));
        desc.push_str(&Self::parser_read_single_value(
            pkg_info.size.as_ref(),
            "ISIZE",
        ));

        desc.push_str(&format!("\n%MD5SUM%\n{}\n", md5sum));

        desc.push_str(&format!("\n%SHA256SUM%\n{}\n", sha256sum));

        desc.push_str(&Self::parser_read_single_value(
            pkg_info.url.as_ref(),
            "URL",
        ));
        desc.push_str(&Self::parser_read_single_value(
            pkg_info.arch.as_ref(),
            "ARCH",
        ));
        desc.push_str(&Self::parser_read_single_value(
            pkg_info.builddate.as_ref(),
            "BUILDDATE",
        ));
        desc.push_str(&Self::parser_read_single_value(
            pkg_info.packager.as_ref(),
            "PACKAGER",
        ));

        desc.push_str(&Self::parser_read_vec(&pkg_info.groups, "GROUPS"));
        desc.push_str(&Self::parser_read_vec(&pkg_info.licenses, "LICENCES"));
        desc.push_str(&Self::parser_read_vec(&pkg_info.replaces, "REPLACES"));
        desc.push_str(&Self::parser_read_vec(&pkg_info.provides, "PROVIDES"));
        desc.push_str(&Self::parser_read_vec(&pkg_info.depends, "DEPENDS"));
        desc.push_str(&Self::parser_read_vec(&pkg_info.optdepends, "OPTDEPENDS"));
        desc.push_str(&Self::parser_read_vec(&pkg_info.makedepends, "MAKEDEPENDS"));
        desc.push_str(&Self::parser_read_vec(
            &pkg_info.checkdepends,
            "CHECKDEPENDS",
        ));

        desc
    }

    fn parser_read_single_value(value: Option<&String>, name: &str) -> String {
        match value {
            Some(v) => format!("\n%{}%\n{}\n", name, v),
            None => "".into(),
        }
    }

    fn parser_read_vec(value: &[String], name: &str) -> String {
        match value.is_empty() {
            true => "".into(),
            false => format!(
                "\n%{}%\n{}",
                name,
                value.iter().map(|v| format!("{}\n", v)).collect::<String>()
            ),
        }
    }

    pub fn into_inner(self) -> std::io::Result<W> {
        self.db.into_inner()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::io::Cursor;
    use std::io::Read;
    use std::path::Path;
    use tar::Archive;

    #[test]
    fn name_from_pkginfo_is_written_to_desc_file() {
        let pkg_info = create_package("foo".into(), "1.0.0".into());
        let mut db = Database::new();
        db.add_package_to_db(pkg_info, "foo-1.0.0.pkg.tar.zst", "bar", "foo")
            .unwrap();

        let archive = Archive::new(Cursor::new(db.into_inner().unwrap()));
        let desc_contents = get_package_desc(archive);

        assert!(
            desc_contents.contains(&format!("%NAME%\n{}", "foo")),
            "actual contents: {}",
            &desc_contents
        );
    }

    #[test]
    fn pkg_version_from_pkginfo_is_written_to_desc_file() {
        let pkg_info = create_package("foo".into(), "1.0.0".into());
        let mut db = Database::new();
        db.add_package_to_db(pkg_info, "foo-1.0.0.pkg.tar.zst", "bar", "foo")
            .unwrap();

        let archive = Archive::new(Cursor::new(db.into_inner().unwrap()));
        let desc_contents = get_package_desc(archive);

        assert!(
            desc_contents.contains(&format!("%VERSION%\n{}", "1.0.0")),
            "actual contents: {}",
            &desc_contents
        );
    }

    #[test]
    fn missing_items_are_not_in_the_file() {
        let pkg_info = create_package("foo".into(), "1.0.0".into());
        let mut db = Database::new();
        db.add_package_to_db(pkg_info, "foo-1.0.0.pkg.tar.zst", "bar", "foo")
            .unwrap();

        let archive = Archive::new(Cursor::new(db.into_inner().unwrap()));
        let desc_contents = get_package_desc(archive);

        assert!(!desc_contents.contains("%BASE%"));
        assert!(!desc_contents.contains("%URL%"));
        assert!(!desc_contents.contains("%DESC%"));
        assert!(!desc_contents.contains("%LICENSE%"));
        assert!(!desc_contents.contains("%BUILDDATE%"));
        assert!(!desc_contents.contains("%PACKAGER%"));
        assert!(!desc_contents.contains("%DEPENDS%"));
        assert!(!desc_contents.contains("%MAKEDEPENDS%"));
    }

    #[test]
    fn pkgbase_from_pkginfo_is_written_to_desc_file() {
        let mut pkg_info = create_package("foo".into(), "1.0.0".into());
        pkg_info.pkgbase = Some(String::from("bar"));

        let mut db = Database::new();
        db.add_package_to_db(pkg_info, "foo-1.0.0.pkg.tar.zst", "bar", "foo")
            .unwrap();
        let archive = Archive::new(Cursor::new(db.into_inner().unwrap()));
        let desc_contents = get_package_desc(archive);

        assert!(
            desc_contents.contains(&format!("%BASE%\n{}", "bar")),
            "actual contents: {}",
            &desc_contents
        );
    }

    #[test]
    fn depends_packages_from_pkginfo_is_written_to_desc_file() {
        let mut pkg_info = create_package("foo".into(), "1.0.0".into());
        pkg_info.depends = vec!["base".into(), "base-devel".into()];

        let mut db = Database::new();
        db.add_package_to_db(pkg_info, "foo-1.0.0.pkg.tar.zst", "bar", "foo")
            .unwrap();
        let archive = Archive::new(Cursor::new(db.into_inner().unwrap()));
        let desc_contents = get_package_desc(archive);

        assert!(
            desc_contents.contains(&format!("%DEPENDS%\n{}\n{}", "base", "base-devel")),
            "actual contents: {}",
            &desc_contents
        );
    }

    #[test]
    fn groups_packages_from_pkginfo_is_written_to_desc_file() {
        let mut pkg_info = create_package("foo".into(), "1.0.0".into());
        pkg_info.groups = vec!["group1".into(), "group2".into()];

        let mut db = Database::new();
        db.add_package_to_db(pkg_info, "foo-1.0.0.pkg.tar.zst", "bar", "foo")
            .unwrap();
        let archive = Archive::new(Cursor::new(db.into_inner().unwrap()));
        let desc_contents = get_package_desc(archive);

        assert!(
            desc_contents.contains(&format!("%GROUPS%\n{}\n{}", "group1", "group2")),
            "actual contents: {}",
            &desc_contents
        );
    }

    #[test]
    fn md5sum_is_written_from_pkginfo_to_desc_file() {
        let pkg_info = create_package("foo".into(), "1.0.0".into());
        let mut db = Database::new();
        db.add_package_to_db(pkg_info, "foo-1.0.0.pkg.tar.zst", "bar", "foo")
            .unwrap();
        let archive = Archive::new(Cursor::new(db.into_inner().unwrap()));
        let desc_contents = get_package_desc(archive);

        assert!(
            desc_contents.contains(&format!("%MD5SUM%\n{}", "bar")),
            "actual contents: {}",
            &desc_contents
        );
    }

    fn get_package_desc(mut archive: Archive<impl Read>) -> String {
        let entry = archive.entries().unwrap().find(|entry| match entry {
            Ok(entry) => entry.header().path().unwrap() == Path::new("foo-1.0.0/desc"),
            _ => panic!("invalid entry"),
        });
        let mut desc_contents = String::new();
        match entry {
            Some(Ok(mut entry)) => entry.read_to_string(&mut desc_contents).unwrap(),
            _ => panic!("invalid entry"),
        };
        desc_contents
    }

    fn create_package(pkgname: String, pkgver: String) -> PkgInfo {
        PkgInfo {
            pkgname,
            pkgbase: None,
            pkgver,
            pkgdesc: None,
            size: None,
            url: None,
            arch: None,
            builddate: None,
            packager: None,
            groups: Vec::new(),
            licenses: Vec::new(),
            replaces: Vec::new(),
            conflicts: Vec::new(),
            provides: Vec::new(),
            depends: Vec::new(),
            optdepends: Vec::new(),
            makedepends: Vec::new(),
            checkdepends: Vec::new(),
        }
    }
}

use actix_web::{get, post, web, App, HttpResponse, HttpServer};
use anyhow::{Context, Result};
use cloudevents::{EventBuilder, EventBuilderV10};
use cloudevents_sdk_actix_web::HttpResponseBuilderExt;
use log::info;
use rusoto_core::region::Region;
use rusoto_s3::{S3Client, S3};
use serde::{Deserialize, Serialize};
use sha2::{Digest, Sha256};
use std::io::{Cursor, Write};
use std::ops::Deref;
use std::sync::Arc;
use tar::Archive;
use tokio::io::AsyncReadExt;
use zstd::stream::decode_all;

mod database;
mod package;

use database::Database;
use package::Package;

#[derive(Serialize, Deserialize, Clone, Debug)]
struct ObjectPutEvent {
    objects: Vec<Object>,
    bucket: Bucket,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
struct Bucket(String);

#[derive(Serialize, Deserialize, Clone, Debug)]
struct Object(String);

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    simple_logger::SimpleLogger::new()
        .with_level(log::LevelFilter::Info)
        .init()
        .unwrap();

    info!("Listening on 0.0.0.0:9001");

    HttpServer::new(|| {
        let s3_client = S3Client::new(Region::Custom {
            name: "eu-west-1".to_owned(),
            endpoint: "http://minio:9000".to_owned(),
        });

        App::new()
            .data(s3_client)
            .wrap(actix_web::middleware::Logger::default())
            .service(handler_endpoint)
            .service(index)
    })
    .bind("0.0.0.0:9001")?
    .workers(1)
    .run()
    .await
}

#[get("/")]
async fn index() -> Result<HttpResponse, actix_web::Error> {
    HttpResponse::Ok().body("test body").await
}

#[post("/")]
async fn handler_endpoint(
    s3_client: web::Data<S3Client>,
    event: web::Json<ObjectPutEvent>,
) -> Result<HttpResponse, actix_web::Error> {
    let s3_client: &S3Client = Arc::deref(web::Data::deref(&s3_client));
    handler(s3_client, &*event).await.unwrap();
    HttpResponse::Ok().await
}

async fn handler(s3_client: &impl S3, event: &ObjectPutEvent) -> anyhow::Result<()> {
    info!("Handler start");

    let mut db = Database::new();

    for object in &event.objects {
        process_record(event.bucket.clone(), object.clone(), s3_client, &mut db).await?;
    }

    let db = zstd::stream::encode_all(
        Cursor::new(
            db.into_inner()
                .with_context(|| "failed to finalise database archive")?,
        ),
        0,
    )
    .with_context(|| "failed to compress database file")?;

    s3_client
        .put_object(rusoto_s3::PutObjectRequest {
            bucket: event.bucket.0.clone(),
            key: "repo.db".to_string(),
            body: Some(db.into()),
            ..Default::default()
        })
        .await
        .with_context(|| "failed writing database to s3 bucket")?;

    Ok(())
}

async fn process_record(
    bucket: Bucket,
    object: Object,
    s3_client: &dyn S3,
    db: &mut Database<impl Write>,
) -> Result<()> {
    info!("{:?} {:?}", &bucket, &object);
    let key = object.0;
    let object = s3_client
        .get_object(rusoto_s3::GetObjectRequest {
            bucket: bucket.0,
            key: key.clone(),
            ..Default::default()
        })
        .await
        .with_context(|| format!("could not fetch package '{}' from s3", &key))?;
    if let Some(body) = object.body {
        let mut buffer = Vec::new();
        body.into_async_read().read_to_end(&mut buffer).await?;

        let md5 = md5::compute(&buffer);
        let mut hasher = Sha256::new();
        hasher.update(&buffer);
        let sha256sum = hasher.finalize();

        let tar_bytes = Cursor::new(
            decode_all(Cursor::new(buffer))
                .with_context(|| format!("invalid compression on package {}", &key))?,
        );
        let package = Package::from_archive(Archive::new(tar_bytes));
        let pkg_info = package
            .pkg_info()
            .with_context(|| format!("invalid package contents {}", &key))?;

        dbg!(format!("{:x}", md5));

        info!("{:#?}", pkg_info);
        db.add_package_to_db(
            pkg_info,
            &key,
            &format!("{:x}", md5),
            &format!("{:x}", sha256sum),
        )
        .with_context(|| format!("failed to append package {} to database", &key))?;
    };
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use rusoto_s3::{CreateBucketRequest, GetObjectRequest, PutObjectRequest, S3};
    use s3mock::S3MockClient;
    use std::io::Read;
    use std::path::Path;
    use tokio::io::AsyncReadExt;

    #[actix_rt::test]
    async fn database_is_created_if_it_does_not_exist_in_the_repo() {
        let event = ObjectPutEvent {
            bucket: Bucket("repo".to_string()),
            objects: vec![],
        };
        let s3_client = S3MockClient::new();
        create_bucket(&s3_client, "repo").await;

        handler(&s3_client, &event).await.unwrap();

        assert!(object_exists(&s3_client, "repo", "repo.db").await);
    }

    #[tokio::test]
    async fn package_in_event_gets_added_to_database() {
        let event = ObjectPutEvent {
            bucket: Bucket("repo".to_string()),
            objects: vec![Object("nvidia-prime-1.0-4-any.pkg.tar.zst".to_string())],
        };

        let s3_client = S3MockClient::new();
        create_bucket(&s3_client, "repo").await;
        put_file(
            &s3_client,
            "repo",
            "tests/nvidia-prime-1.0-4-any.pkg.tar.zst",
        )
        .await;

        handler(&s3_client, &event).await.unwrap();

        assert!(
            get_package_desc_from_db(&s3_client, "repo", "repo.db", "nvidia-prime-1.0-4")
                .await
                .is_some()
        );
    }

    async fn create_bucket(s3_client: &impl S3, bucket: impl Into<String>) {
        s3_client
            .create_bucket(CreateBucketRequest {
                bucket: bucket.into(),
                ..Default::default()
            })
            .await
            .expect("Failed to create test bucket");
    }

    async fn put_file(s3_client: &impl S3, bucket: impl Into<String>, filename: impl AsRef<Path>) {
        s3_client
            .put_object(PutObjectRequest {
                bucket: bucket.into(),
                key: filename
                    .as_ref()
                    .file_name()
                    .unwrap()
                    .to_str()
                    .unwrap()
                    .into(),
                body: Some(tokio::fs::read(filename.as_ref()).await.unwrap().into()),
                ..Default::default()
            })
            .await
            .expect("Failed to add object to bucket");
    }

    async fn object_exists(
        s3_client: &impl S3,
        bucket: impl Into<String>,
        key: impl Into<String>,
    ) -> bool {
        match s3_client
            .get_object(GetObjectRequest {
                bucket: bucket.into(),
                key: key.into(),
                ..Default::default()
            })
            .await
        {
            Ok(_) => true,
            Err(_) => false,
        }
    }

    async fn get_object(
        s3_client: &impl S3,
        bucket: impl Into<String>,
        key: impl Into<String>,
    ) -> Vec<u8> {
        let body = s3_client
            .get_object(GetObjectRequest {
                bucket: bucket.into(),
                key: key.into(),
                ..Default::default()
            })
            .await
            .unwrap()
            .body
            .unwrap();
        let mut buffer = Vec::new();
        body.into_async_read()
            .read_to_end(&mut buffer)
            .await
            .unwrap();
        buffer
    }

    async fn get_package_desc_from_db(
        s3_client: &impl S3,
        bucket: &str,
        repo: &str,
        package: &str,
    ) -> Option<String> {
        let db_archive = Cursor::new(get_object(s3_client, bucket, repo).await);
        let db_archive = Cursor::new(zstd::decode_all(db_archive).unwrap());
        Archive::new(db_archive)
            .entries()
            .unwrap()
            .find(|entry| {
                entry.as_ref().unwrap().header().path().unwrap() == Path::new(package).join("desc")
            })
            .map(|entry| {
                let mut s = String::new();
                entry.unwrap().read_to_string(&mut s).unwrap();
                s
            })
    }
}

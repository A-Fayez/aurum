import * as pulumi from "@pulumi/pulumi";
import * as aws from "@pulumi/aws";
import * as awsx from "@pulumi/awsx";

// Create an AWS resource (S3 Bucket)
const bucket = new aws.s3.Bucket("repository");

// Configure IAM so that the AWS Lambda can be run.
const repoDbRole = new aws.iam.Role("repo-db", {
   assumeRolePolicy: {
      Version: "2012-10-17",
      Statement: [{
         Action: "sts:AssumeRole",
         Principal: {
            Service: "lambda.amazonaws.com",
         },
         Effect: "Allow",
         Sid: "",
      }],
   },
});

new aws.iam.RolePolicyAttachment("repo-db-AWSLambdaFullAccess", {
   role: repoDbRole,
   policyArn: aws.iam.ManagedPolicies.AWSLambdaFullAccess,
});

// Next, create the Lambda function itself:
const repoDbLambda = new aws.lambda.Function("repo-db", {
   runtime: "provided",
   role: repoDbRole.arn,
   handler: "handler",
   code: new pulumi.asset.FileArchive("target/x86_64-unknown-linux-musl/release/repo-db.zip"),
});

// Finally, register the Lambda to fire when a new Object arrives:
bucket.onObjectCreated("docsHandler", repoDbLambda);

// Export the name of the bucket
export const bucketName = bucket.id;
